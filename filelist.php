<?php
    session_start();
    if(!isset($_SESSION['usr']) || !isset($_SESSION['pswd']) || !isset($_SESSION['id'])) {
        $emptyarr = array();
        echo json_encode($emptyarr);
        exit;
    }
    header("Content-Type: application/json;");
    include("mp3.class.php");

    class CMP3File { 
        
        function getid3 ($file) {
            $arr = array();
            $arr['location']     = $file;
            if (file_exists($file)) {
                $id_start   = filesize($file) - 128;
                $fp         = fopen($file,"r");
                fseek($fp, $id_start);
                $tag    = fread($fp , 3);
                if ($tag == "TAG") {
                    $arr['title']    = jsonRemoveUnicodeSequences(fread($fp,30));
                    $arr['artist']   = jsonRemoveUnicodeSequences(fread($fp,30));
                    $arr['album']    = jsonRemoveUnicodeSequences(fread($fp,30));
                    $arr['year']     = jsonRemoveUnicodeSequences(fread($fp,4));
                    $arr['comment']  = jsonRemoveUnicodeSequences(fread($fp,30));
                    $arr['genre']    = jsonRemoveUnicodeSequences(fread($fp,1));
                    fclose($fp);                    
                } else {
                    fclose($fp);
                }
            }
            return $arr;
        }
    }

    function jsonRemoveUnicodeSequences($struct) {
       // return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($struct));
        return preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $struct);
    }

    function clean($elem) 
    { 
        if(!is_array($elem)) 
            $elem = htmlentities($elem,ENT_QUOTES,"UTF-8"); 
        else 
            foreach ($elem as $key => $value) 
                $elem[$key] = clean($value); 
        return $elem; 
    } 

    function listMp3Songs($dir)
    {
        $files = glob("$dir/*.[mM][pP]3");
        return $files;
    }
    
    $arr = array();
    $_DIR['GET'] = clean($_GET);
    
    foreach($_DIR['GET'] as &$f)
    {
        $arr = array_merge($arr, listMp3Songs($f));
    }

    if(isset($_GET["full_detail"]) && $_GET["full_detail"] == "true")
    {
        $fullDetail = array();
        $met = new CMP3File();
        foreach($arr as $value)
        {
            $m = new mp3file($value);
            $a = $m->get_metadata();
            $metadata = array_merge($met->getid3($value), $a);

            // print_r($metadata);
            // echo json_encode($metadata);
            array_push($fullDetail, $metadata);
            // echo "<br><br>";
        }
        $fullDetail = json_encode($fullDetail);
        // $fullDetail = str_replace("\0", "", $fullDetail);

        // $fullDetail = jsonRemoveUnicodeSequences($fullDetail);

        echo $fullDetail;
    } else {
        echo json_encode($arr);    
    }
    
    /*
     * if(isset($_GET['dir']))
    {
        echo json_encode(listMp3Songs($_GET['dir']));
    }
    else
    {
        echo "Cannot list files";
    }*/
?>
