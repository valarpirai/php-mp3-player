<?php
	session_start();

	$response 	= array();
	$passhash 	= "";
	$usrid 		= 0;
	$email;
	$verify;

	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		function createUser()
		{	
			$db = new SQLite3('sqlite');

			$data = array(
				'name' => $_POST['name'],
				'pass' => $_POST['password'],
				'email' => $_POST['email']
			);
			
			$nameStr	= $data['name'];
			$passString = $data['pass'];
			$passhash	= hash('sha256', $passString);
			$verify		= rand(100 , 10000);
			
			$stmt = "insert into users(name,email,passwd,verified) values('" . $data['name'] . "','" . $data['email'] . "','" . $passhash . "', $verify);";
			$result = $db->exec($stmt);
			
			$query = "SELECT * FROM users WHERE name like '$nameStr' and passwd like '$passhash';";

			$stmt = $db->prepare($query);
			$results = $stmt->execute();

			$row 	= $results->fetchArray();
			
			$email  = $row['email'];
			$usrid 	= $row['id'];

			$_SESSION['usr'] 	= $_POST['name'];
			$_SESSION['pswd']	= $passhash;
			$_SESSION['id']		= $usrid;
			$_SESSION['code']	= $row['verified'];

			return $usrid > 0 ? true : false;
		}
		
		$response['status'] = createUser();
		$response['message'] = $response['status'] == true
				? 'User created with username :' . $_POST['name']
				: 'There was a problem in creating user.' . $_POST['name'];
		
		if($response['status'] == true) {
			header("Location: sendVerificationLink.php");
		} else {
			header('Location: login.html');
		}
		exit;
	}else {
		header('Location: login.html');
	}
?>
