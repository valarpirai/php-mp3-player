# php mp3 Player #

## Description ##

This is a PHP web application to host mp3 songs on a apache web server.

Requirements:

 * apache2
 * php5
 * php5-sqlite
 * git
 * perl
 * sendmail

change shell in /etc/passwd for apache user
 
### How do I get set up? ###

* Dependencies
    * apache2
    * php5.5

* Summary of set up
    * git clone https://bitbucket.org/valarpirai/php-mp3-player.git
            or
    * Download this repository

    * Copy all the files to apache directory (Linux -> "/var/www/mp/")
    * Copy your mp3 songs to the directory(folder) named "Folder"
    * Open this URL in browser(replace <server-ip> your server IP), http://<server-ip>/mp/
