<?php
	session_start();
	if(!isset($_SESSION['usr']) || !isset($_SESSION['pswd']) || !isset($_SESSION['id']) ) {
		header("Location: login.php");
		exit;
	}
	
	class Dir
	{
		public $text = "";
		public $children  = array();
	}
	
	class JsTree
	{
		public $id = "";
		public $text = "";
		public $parent  = "";
	}
		
	function listDir($dir)
	{
		$obj = new Dir();
		$obj->text = $dir;
		$dirs = array_filter(glob("$dir/*"), 'is_dir');
		foreach($dirs as &$f)
		{
			array_push($obj->children, listDir($f));
		}
		return $obj;
	}

	function listDirectory($dir, $parent='#')
	{
		$arr = array();
		
		$obj = new JsTree();
		$obj->text = end(split("/", $dir));
		$obj->id = $dir;
		$obj->parent = $parent;
		
		array_push($arr, $obj);
		
		$dirs = array_filter(glob("$dir/*"), 'is_dir');
		foreach($dirs as &$f)
		{
			$arr = array_merge($arr, listDirectory($f, $dir));
		}
		return $arr;
	}
	
	function listMp3Songs($dir)
	{
		$files = glob("$dir/*.mp3");
		return $files;
	}
	
?>
<html lang="en">
<head>
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/dashboard.css" />
	<link rel="stylesheet" href="css/style.min.css" />

	<script src='js/jquery-1.10.2.js'></script>
	<script src='js/bootstrap.min.js'></script>
	<script src="js/jstree.min.js"></script>

	<script type="text/javascript">
		var currentDirectory, Songlist;
		var EmailVerified = <?php echo $_SESSION['code'] == 0 ? "true" : "false"; ?> ;
		$(document).ready(function() {
			if(EmailVerified == true)
			{
				$(".alert").alert();
				$('div#email-not-verified').addClass("hide");
			}
		});
	</script>
</head>
<body>
	<div class="container-fluid">
	<div class="row header">
		<div class="col-md-2">
			<span class="titletext">MP3 Player</span>
		</div>
		<div class="col-md-4 middle">
			Currently playing : <label id='currentSong'></label>
		</div>
		<div class="col-md-6 middle">
			<div class="collapse navbar-collapse bs-example-js-navbar-scrollspy">
				<div class="left-float">
					<audio id="audioplayer" controls autoplay src="" onended="PlayNext();" type="audio/mpeg">Your browser does not support the audio tag.</audio>
					<img id="previous_song" class="control" src="img/previous_song.png"/>
					<img id="next_song" class="control" src="img/next_song.png"/>
					<button id="selectall" class="btn btn-success">Select All</button>
				</div>

				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" id="navbarDrop1" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['usr']; ?><span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="navbarDrop1">
							<li><a href="#" class="uploadsongs">Upload Songs</a></li>
							<li><a href="#" class="show-playlist">Show Playlist</a></li>
							<li><a href="#" class="show-directorylist">Browse song</a></li>
							<li class="divider"></li>
							<li><a href="logout.php">Logout</a></li>
						</ul>
					</li>
				</ul>

			</div>
		</div>
	</div>
	<div id="email-not-verified" class="row no-margin">
		<div class="alert alert-warning fade in" role="alert">
			Hi <?php echo $_SESSION['usr']; ?>, you don't have a verified Email address.
			Please <strong>verify email</strong> address by clicking the link sent to your email address.
		</div>
	</div>
	
	<div class="row content">
		<div class="col-md-3">
			<div id="tree" class="directorylist-div styled-border"></div>
			<div id="usrplaylist" class="playlist-div styled-border hide"></div>
		</div>
	<div class="col-md-9">
	<div class="song-play">
		<div id="songs">
			<ul id="playlist" class="songlist"></ul></div>
		</div>
	  </div>
	</div>
  </div>
</body>

<!-- Large modal -->
<div class="modal fade" id="shortcutKeys" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="myModalLabel">Keyboard Shortcuts</h4>
		</div>
		<div class="modal-body">
			<table>
				<tr>
					<td>
						<span class="key">p</span>
					</td>
					<td>
						<span >Play / Pause</span>
					</td>
				</tr>
				<tr>
					<td>
						<span class="key">n</span>
					</td>
					<td>
						<span >Play Next</span>
					</td>
				</tr>
				<tr>
					<td>
						<span class="key">b</span>
					</td>
					<td>
						<span >Play Previous</span>
					</td>
				</tr>
				<tr>
					<td>
						<span class="key">?</span>
					</td>
					<td>
						<span >Show Help</span>
					</td>
				</tr>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
    </div>
  </div>
</div>


<!-- Large modal -->
<div class="modal fade" id="uploadSongs" tabindex="-1" role="dialog" aria-labelledby="myUploadSongsLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="myModalLabel">Upload Songs to Server</h4>
		</div>
		<div class="modal-body">
			<iframe class="fileupload-container" src="file-upload/index.html"></iframe>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
    </div>
  </div>
</div>

<script type='text/javascript'>
	var SelectedSongList = new Array();
	var CurrentIndex = 0;
	directoryChanged = false;

	$(function() {
		var json_data = { 'data' : <?php echo json_encode(listDirectory('./Folder')); ?>};
		json_data['data'][0].state = { 'opened' : true, 'selected' : true};
		// console.log(json_data);

		$('#tree').jstree({'core' : json_data});

		$('#tree').on("changed.jstree", function (e, data) {
			CurrentIndex = 0;
			directoryChanged = true;
			// console.log(data.selected);
			var selectedDirs = data.selected;
			
			// http://192.168.1.26/mp/filelist.php?dir=go&dir1=3d
			var filelistURL = "filelist.php?";
			
			for(i = 0; i < selectedDirs.length; i++)
			{
				var currentDirectory = selectedDirs[i];
				filelistURL += "dir" + i + "=" + currentDirectory + "&";
			}
			$.get(filelistURL, function(data) {
				listMp3files(data);
			});
		});

		$('#tree').click(function() {
			$(this).blur();
		});

		function listMp3files(songs)
		{
			// songs = JSON.parse(songs);
			// console.log(songs);
			Songlist = songs;
			var songList = $('#playlist');
			
			$('#selectall').attr('value', "select All");
			$('#selectall').text("select All");
			songList.children().remove();
			
			for(var i = 0; i < songs.length; i++)
			{
				var song = songs[i];
				var name = song.split("\/");
				name = name[name.length - 1];
				var cut_name = name.substring(0, 36);
				var str = "<li audiourl='" + song + "'><div class='chkbox'><input type='checkbox' id='" + song + "' class='chk'/>" +
					"<label class='chklabel' for='" + song + "'></label></div><label class='chktextlabel chklabel songname' title='" +
					name + "' id='" + song + "' data-toggle='tooltip' data-placement='top'>" + cut_name + "</label></li>";
				songList.append(str);
			}
		$('.chktextlabel').tooltip();
		}
		
		$("label").on("click", function() {
			$("#audioplayer").attr({
				"src": $(this).attr("id"),
				"autoplay": "autoplay"
			});
			setCurrentSongName($(this).attr("id"));
		});
		
		$('#next_song').click(function()
		{
			if(0 >= SelectedSongList.length)
			{
				if(contructPlayList())
					PlayNext();
			} else
				PlayNext();
		});
		
		$('#previous_song').click(function()
		{
			if(0 >= SelectedSongList.length)
			{
				if(contructPlayList())
					PlayPrevious();
			}else
				PlayPrevious();
		});
		
		$('#selectall').click(function() {
			if("Unselect All" != $(this).attr('value'))
			{
				$('.chk').prop('checked', true);
				$(this).attr('value', "Unselect All");
				$(this).text("Unselect All");
				contructPlayList();
				SetSong();
			}else
			{
				$('.chk').prop('checked', false);
				$(this).attr('value', "Select All");
				$(this).text("Select All");
				SelectedSongList = [];
			}
			$(this).blur();
		});
		
		delegator("input.chk", 'change');
		delegator("label.songname", 'click');
		delegator("#audioplayer", 'ended');
	});

	function SetSong()
	{
		if(1 > SelectedSongList.length)
		{
			CurrentIndex = 0;
			return "";
		}
		
		if($("#audioplayer").get(0).paused)	{
			CurrentIndex = 0;
			$("#audioplayer").attr({
				"src": SelectedSongList[CurrentIndex]
			});
			$("#audioplayer").get(0).pause();
			setCurrentSongName(SelectedSongList[CurrentIndex]);
		}
	}

	function delegator(selector, events)
	{
		$('body').on(events, selector, function( event ) {
			// console.log(event);
			// console.log($(this));
			if(event.type == "change")
			{
				contructPlayList();
				
				if((!$("#audioplayer").attr("src") || directoryChanged) && $("#audioplayer").get(0).paused)
				{
					directoryChanged = false;
					var song_name = SelectedSongList[0];
					$("#audioplayer").attr({
						"src": song_name,
						"autoplay": "autoplay"
					});
					$("#audioplayer").get(0).pause();
					setCurrentSongName(song_name);
				}
				$(this).blur();
			}else if(event.type == "click")
			{
				var song_name = $(this).attr("id");
				$("#audioplayer").attr({
					"src": song_name,
					"autoplay": "autoplay"
				});
				setCurrentSongName(song_name);
				console.log(song_name);
				
				var l = SelectedSongList.length;
				for(var i=0; i < l; i++)
				{
					if(song_name == SelectedSongList[i])
						CurrentIndex = i;
				}
				$(this).blur();
			}else if(event.type == "ended")
			{
				PlayNext();
			}
		});
	}

	function setCurrentSongName(name_str)
	{
		var name = name_str.split("\/");
		name = name[name.length - 1 ];
		$('#currentSong').text(name);
		document.title = name;
	}

	function PlayNext()
	{
		CurrentIndex++;
		play();
	}

	function PlayPrevious()
	{
		CurrentIndex--;
		play();
	}

	function play()
	{
		if(SelectedSongList.length < 1)
		{
			CurrentIndex = 0;
			alert("Select at leaset one song");
			return "";
		}else if(CurrentIndex >= SelectedSongList.length)
		{
			CurrentIndex = 0;
		}
		
		$("#audioplayer").attr({
			"src": SelectedSongList[CurrentIndex],
			"autoplay": "autoplay"
		});
		setCurrentSongName(SelectedSongList[CurrentIndex]);
	}

	function contructPlayList()
	{
		SelectedSongList = new Array();
		$('#playlist').find('input').each(function() {
			if($(this).is(':checked'))
			{
				var song = $(this).attr('id');
				SelectedSongList.push(song);
			}
		});
		
		if(!Songlist || Songlist.length < 1)
		{
			alert("Select directories from the tree");
			return false;
		}
		return true;
	}

	$(document).keypress(function(event) {
		event.preventDefault();
		console.log( "Handler for .keypress() called." + event.which);
		// Next Song
		if(event.which == 115 || event.which == 83)
		{
			$('#selectall').click();
		}else if(event.which == 110 || event.which == 78)
		{
			if(0 >= SelectedSongList.length)
			{
				if(contructPlayList())
					PlayNext();
			} else
				PlayNext();
		}else if(event.which == 98 || event.which == 66)
		{
			if(0 >= SelectedSongList.length)
			{
				if(contructPlayList())
					PlayPrevious();
			} else
				PlayPrevious();
		}else if(event.which == 32 || event.which == 112 || event.which == 80)
		{
			if(!$("#audioplayer").get(0).paused)
				$("#audioplayer").get(0).pause();
			else
				$("#audioplayer").get(0).play();
		}else if(event.which == 63)
		{
			showHelpModal();
		}
		return true;
	});

	function showHelpModal()
	{
		$('#shortcutKeys').modal("show");
	}

	$(".uploadsongs").click(function() {
		$('#uploadSongs').modal("show");
	});

	$(window).resize(function() {
		// $(".content").width($(window).width());
		$(".content").height(window.innerHeight - 90);
	});

	$(".content").height(window.innerHeight - 90);
	document.title = "<php> mp3 Player";

	$(".show-playlist").click(function(){
		$(".directorylist-div").addClass("hide");
		$(".playlist-div").removeClass("hide");
	});

	$(".show-directorylist").click(function(){
		$(".directorylist-div").removeClass("hide");
		$(".playlist-div").addClass("hide");
	});
</script>
</html>
