<?php
	session_start();
	if(!isset($_SESSION['usr']) || !isset($_SESSION['pswd']) || !isset($_SESSION['id'])) {
		header("Location: login.php");
		echo "Session not init";
		exit;
	}

	$db = new SQLite3('sqlite');

	$id					= $_SESSION['id'];
	$verficationCode 	= $_SESSION['code'];

	$query = "SELECT * FROM users WHERE id = $id and verified = $verficationCode;";

	$stmt = $db->prepare($query);
	$results = $stmt->execute();
	
	$rows 	= $results->fetchArray();
	$db->close();
	if(count($rows) < 1)
	{
		echo "No Rows";
		// header("Location: login.php");
		exit;
	}

	$row 	= $rows;
	$email  = $row['email'];
	
	$verificationLink = "http://$_SERVER[HTTP_HOST]/mp/verify?uid=$id&code=$verficationCode";

	$subject 	= "Please confirm your email";
	$message	= "Hi " . $row['name'] . "," . "\nOpen the following link to verify your Email account.\n\n$verificationLink";
	
	$content_file = "/tmp/$verficationCode.txt";

	// $file = fopen("$content_file","w");
	// fwrite($file,"$message");
	// fclose($file);

	// $mail_command = "/usr/sbin/sendmail -s \"$subject\" $email < $content_file && rm $content_file &";
	// exec("$mail_command");

	$mail = mail($email, $subject, $message);
	if($mail) {
		echo "Thank you for using our mail form";
	} else {
		echo "Mail sending failed."; 
	}
	header("Location: login.php");
	exit;
?>